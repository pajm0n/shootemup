﻿using Entitas;
using UnityEngine;

public class DestroyAfterTimeSystem : IExecuteSystem
{
    private readonly IGroup<GameEntity> group;

    public DestroyAfterTimeSystem(Contexts contexts)
    {
        group = contexts.game.GetGroup(GameMatcher.AllOf(
            GameMatcher.DestroyAfterTime
        ));
    }

    public void Execute()
    {
        foreach (GameEntity e in group)
        {
            ProccessEntity(e);
        }
    }

    private void ProccessEntity(GameEntity e)
    {
        e.destroyAfterTime.time -= Time.deltaTime;
        if (e.destroyAfterTime.time <= 0.0f)
        {
            e.isDestroy = true;
        }
    }
}