﻿using System.Collections.Generic;
using Entitas;
using UnityEngine;

public class ObjectWithLifeCollisionSystem : ReactiveSystem<GameEntity>
{
    public ObjectWithLifeCollisionSystem(Contexts contexts) : base(contexts.game)
    {
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.AllOf(GameMatcher.Life, GameMatcher.Collided));
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.hasCollided && entity.hasLife;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (GameEntity e in entities)
        {
            if (e.collided.other.hasDamage)
            {
                LifeStruct newLife = e.life.life;
                newLife.currentLife -= e.collided.other.damage.damage;
                e.ReplaceLife(newLife);
            }

            e.RemoveCollided();
        }
    }
}