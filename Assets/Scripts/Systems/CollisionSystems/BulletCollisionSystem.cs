﻿using System.Collections.Generic;
using Entitas;

namespace Systems.CollisionSystems
{
    public class BulletCollisionSystem : ReactiveSystem<GameEntity>
    {
        public BulletCollisionSystem(Contexts contexts) : base(contexts.game)
        {
        }

        protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
        {
            return context.CreateCollector(GameMatcher.AllOf(GameMatcher.Bullet, GameMatcher.Collided));
        }

        protected override bool Filter(GameEntity entity)
        {
            return entity.isBullet && entity.hasCollided;
        }

        protected override void Execute(List<GameEntity> entities)
        {
            foreach (GameEntity e in entities)
            {
                e.isDestroy = true;
            }
        }
    }
}