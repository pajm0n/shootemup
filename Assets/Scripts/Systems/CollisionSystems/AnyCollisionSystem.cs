﻿using System.Collections.Generic;
using UnityEngine;
using Entitas;

public class AnyCollisionSystem : ReactiveSystem<GameEntity>
{
    private Contexts _contexts;
    public AnyCollisionSystem(Contexts contexts) : base(contexts.game)
    {
        _contexts = contexts;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.Collision);
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.hasCollision;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            GameObject first = entity.collision.firstObject;
            GameObject second = entity.collision.secondObject;

            GameEntity firstEntity = _contexts.game.GetEntitiesWithView(first).SingleEntity();
            GameEntity secondEntity = _contexts.game.GetEntitiesWithView(second).SingleEntity();

            ProccessCollision(firstEntity, secondEntity);
            entity.isDestroy = true;
        }
    }

    private void ProccessCollision(GameEntity first, GameEntity second)
    {
       first.ReplaceCollided(second);
       second.ReplaceCollided(first);
    }
}
