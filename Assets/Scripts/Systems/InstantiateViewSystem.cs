﻿using UnityEngine;
using Entitas;
using Entitas.Unity;
using System.Collections.Generic;

public class InstantiateViewSystem : ReactiveSystem<GameEntity>
{
    private readonly Contexts contexts;

    public InstantiateViewSystem(Contexts contexts) : base(contexts.game)
    {
        this.contexts = contexts;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.Resource);
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.hasResource;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            GameObject gameObject = Object.Instantiate(entity.resource.prefab);
            entity.AddView(gameObject);
            gameObject.Link(entity);
        }
    }
}