﻿using Entitas;

public class ReplaceForwardSystem : IExecuteSystem
{
    private readonly Contexts contexts;
    private readonly IGroup<GameEntity> group;


    public ReplaceForwardSystem(Contexts contexts)
    {
        this.contexts = contexts;

        group = contexts.game.GetGroup(GameMatcher.AllOf(
            GameMatcher.View, GameMatcher.Forward
        ));
    }

    public void Execute()
    {
        foreach (GameEntity spawn in group)
        {
            spawn.ReplaceForward(spawn.view.View.gameObject.transform.forward);
        }
    }
}
