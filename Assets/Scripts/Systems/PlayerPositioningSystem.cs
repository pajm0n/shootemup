﻿using System.Collections.Generic;
using Entitas;
using UnityEngine;


public class PlayerPositioningSystem : ReactiveSystem<InputEntity>, ICleanupSystem
{
    private Contexts contexts;
    private GameEntity playerEntity;
    private InputEntity inputEntity;

    private const float MIN_DETECTION_FROM_MOUSE = 0.3f;
    private const float CLAM_INPUT_VALUE = 1f;

    public PlayerPositioningSystem(Contexts contexts) : base(contexts.input)
    {
        this.contexts = contexts;
    }

    protected override ICollector<InputEntity> GetTrigger(IContext<InputEntity> context)
    {
        return context.CreateCollector(InputMatcher.Input);
    }

    protected override bool Filter(InputEntity entity)
    {
        return entity.hasInput;
    }

    protected override void Execute(List<InputEntity> entities)
    {
        playerEntity = contexts.game.playerEntity;
        inputEntity = contexts.input.inputEntity;

        if (playerEntity == null)
            return;


        Vector2 playerPosition2D = new Vector2(playerEntity.position.Position.x, playerEntity.position.Position.y);
        Vector2 inputPosition2D = inputEntity.input.value;

        if (Vector2.Distance(playerPosition2D, inputPosition2D) > MIN_DETECTION_FROM_MOUSE)
        {
            Vector2 direction = inputPosition2D - playerPosition2D;
            
            direction.y = 0;
            direction.x = Mathf.Clamp(direction.x, -CLAM_INPUT_VALUE, CLAM_INPUT_VALUE);
              
            playerEntity.ReplaceDirection(direction);
            playerEntity.velocity.speed = contexts.game.playerConfig.value.playerSpeed;
        }

    }

    public void Cleanup()
    {
        if (contexts.game.playerEntity == null)
            return;

        contexts.game.playerEntity.velocity.speed = 0;

    }
}