﻿using System.Runtime.Serialization.Formatters;
using Entitas;
using UnityEngine;

public class DestroyOutOfBounds : IExecuteSystem
{
    private readonly IGroup<GameEntity> group;
    private readonly Camera camera;
    private float offset;

    public DestroyOutOfBounds(Contexts contexts)
    {
        camera = Camera.main;
        group = contexts.game.GetGroup(GameMatcher.AllOf(
            GameMatcher.Position
        ));
    }

    public void Execute()
    {

        foreach (GameEntity e in group)
        {
            Vector3 screenPoint = camera.WorldToViewportPoint(e.position.Position);
            
            if (OutOfYBoundary(screenPoint) || OutOfXBoundary(screenPoint))
            {
                e.isDestroy = true;
            }
        }
    }

    private bool OutOfYBoundary(Vector3 screenPoint)
    {
        return screenPoint.y < -1 || screenPoint.y > 2;
    }

    private bool OutOfXBoundary(Vector3 screenPoint)
    {
        return screenPoint.x > 1.3f || screenPoint.x < -1.3f;
    }
}