﻿using Entitas;

namespace Systems.MovementSystems
{
    public class MovementLoopPathSystem : IExecuteSystem
    {
        private IGroup<GameEntity> bossGroup;

        public MovementLoopPathSystem(Contexts contexts)
        {
            bossGroup = contexts.game.GetGroup(GameMatcher.AllOf(
                GameMatcher.LoopingMovement, 
                GameMatcher.EnemyPath));
        }


        public void Execute()
        {
            if (bossGroup.count <= 0)
                return;

            foreach (var e in bossGroup)
            {
                if (e.enemyPath.isLastMovement)
                {
                    e.enemyPath.distanceTraveled = 0;
                }
            }
        }
    }
}