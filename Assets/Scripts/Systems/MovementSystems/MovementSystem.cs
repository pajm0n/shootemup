﻿using Entitas;
using UnityEngine;

public class MovementSystem : IExecuteSystem
{
    private readonly IGroup<GameEntity> group;

    public MovementSystem(Contexts contexts)
    {
        group = contexts.game.GetGroup(GameMatcher.AllOf(
            GameMatcher.Direction,
            GameMatcher.Velocity,
            GameMatcher.Position,
            GameMatcher.View));
    }

    public void Execute()
    {
        float time = Time.deltaTime;
        foreach (var e in group)
        {
            e.position.Position =
                e.position.Position 
                + e.direction.Direction 
                * e.velocity.speed * time;

            e.view.View.transform.position = e.position.Position;
        }
    }
}
