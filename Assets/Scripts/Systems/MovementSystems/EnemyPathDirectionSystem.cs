﻿using Entitas;
using UnityEngine;

public class EnemyPathDirectionSystem : IExecuteSystem
{
    
    private readonly IGroup<GameEntity> group;

    public EnemyPathDirectionSystem(Contexts contexts)
    {
        group = contexts.game.GetGroup(GameMatcher.AllOf(
            GameMatcher.Velocity,
            GameMatcher.Position,
            GameMatcher.View,
            GameMatcher.EnemyPath));
        //Enemy is not boss since the boss may repeat or stay in place
    }

    public void Execute()
    {
        float time = Time.deltaTime;
       
        foreach (var e in group)
        {
            Vector3 dir = e.enemyPath.GetMovementDirection(e.velocity.speed * time, e.position.Position);
            e.ReplaceDirection(e.enemyPath.isLastMovement? e.enemyPath.GetLastDirection() : dir);
        }       
    }
}