﻿using Entitas;

namespace Systems.MovementSystems
{
    public class EnemyChangableSpeedSystem : IExecuteSystem
    {
        private readonly IGroup<GameEntity> group;

        public EnemyChangableSpeedSystem(Contexts contexts)
        {
            group = contexts.game.GetGroup(GameMatcher.AllOf(
                GameMatcher.ChangableSpeed,
                GameMatcher.Position,
                GameMatcher.View,
                GameMatcher.EnemyPath));
        }
        
        public void Execute()
        {
            EnemyPathComponent path;
            
            foreach (var e in group)
            {
                path = e.enemyPath;
                e.ReplaceVelocity(e.changableSpeed.
                    GetCurrentSpeed(path.distanceTraveled, path.WholeDistanceToTravel));
            }
        }
    }
}