﻿using Entitas;
using UnityEngine;


public class InputGatheringSystem : IExecuteSystem
{
    private Contexts contexts;
    private bool ispressed = false;
    private Camera cam;

    public InputGatheringSystem(Contexts contexts)
    {
        this.contexts = contexts;
        cam = Camera.main;
    }


    public void Execute()
    {
        if (Input.GetMouseButton(0))
        {
            Vector3 mousePos = Input.mousePosition;
            mousePos.z = cam.transform.position.z * -1;
            
            Vector3 position = cam.ScreenToWorldPoint(mousePos);
            contexts.input.ReplaceInput(position);
        }
    }
}