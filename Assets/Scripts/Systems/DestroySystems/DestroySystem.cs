﻿using UnityEngine;
using Entitas;
using Entitas.Unity;
using System.Collections.Generic;

public class DestroySystem : ReactiveSystem<GameEntity>
{
    private readonly Contexts contexts;

    public DestroySystem(Contexts contexts) : base(contexts.game)
    {
       this.contexts = contexts;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.Destroy);
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.isDestroy;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            if (entity.hasView)
            {
                GameObject view = entity.view.View;
                view.Unlink();
                Object.Destroy(view);
            }
            entity.Destroy();
        }
    }
}