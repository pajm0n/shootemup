﻿using Entitas;
using UnityEngine;

namespace Systems.DestroySystems
{
    public class OnEnemyWithPointsDestroyed : IExecuteSystem
    {
        private readonly IGroup<GameEntity> group;
        private readonly GameContext gameContext;
        
        public OnEnemyWithPointsDestroyed(Contexts contexts)
        {
            group = contexts.game.GetGroup(GameMatcher.AllOf(
                GameMatcher.Destroy, GameMatcher.Enemy,
                GameMatcher.PointsOnDeath, GameMatcher.DestroyedByPlayer));
           
            gameContext = contexts.game;
        }

        public void Execute()
        {
            if(group.count == 0)
                return;

            foreach (GameEntity e in group)
            {
                gameContext.ReplaceAccumulatedPoints(
                    gameContext.accumulatedPoints.value + e.pointsOnDeath.value);
            }
        }
    }
}