﻿using UnityEngine;
using Entitas;

public class ShotgunBulletSpawnSystem : IExecuteSystem
{
    private readonly Contexts contexts;
    private readonly IGroup<GameEntity> group;

    private float currentTime;

    public ShotgunBulletSpawnSystem(Contexts contexts)
    {
        this.contexts = contexts;

        group = contexts.game.GetGroup(GameMatcher.AllOf(
            GameMatcher.Position, GameMatcher.ShotgunBulletSpawn, GameMatcher.Forward,
            GameMatcher.LastShootTime
        ));

        currentTime = 0.0f;
    }

    public void Execute()
    {
        currentTime += Time.deltaTime;
        foreach (GameEntity spawn in group)
        {
            if (CouldSpawnBullet(spawn))
            {
                SpawnBullets(spawn);
                spawn.ReplaceLastShootTime(currentTime);
            }
        }
    }

    private bool CouldSpawnBullet(GameEntity entity)
    {
        float cooldown = GetCooldown(entity);
        return currentTime > entity.lastShootTime.value + cooldown;
    }

    private float GetCooldown(GameEntity entity)
    {
        if (entity.isPlayer)
        {
            return 0.5f;
        }
        return entity.hasBulletCooldown ? entity.bulletCooldown.cooldown : 0.5f;
    }

    private void SpawnBullets(GameEntity spawnEntity)
    {
        IBulletBuilder builder = new ForwardBulletBuilder();
        builder.SetContext(contexts);
        builder.SetCreator(spawnEntity);

        for(int i = -1; i < 2; i++) { 
            GameEntity newBullet = contexts.game.CreateEntity();
            builder.Build(newBullet);
            newBullet.ReplaceDirection(Quaternion.AngleAxis(i * 45, Vector3.forward) * newBullet.forward.value);
        }

    }
}
