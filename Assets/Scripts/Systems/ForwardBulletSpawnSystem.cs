﻿using UnityEngine;
using Entitas;

public class ForwardBulletSpawnSystem : IExecuteSystem
{
    private readonly Contexts contexts;
    private readonly IGroup<GameEntity> group;

    private float currentTime;

    public ForwardBulletSpawnSystem(Contexts contexts)
    {
        this.contexts = contexts;

        group = contexts.game.GetGroup(GameMatcher.AllOf(
            GameMatcher.Position, GameMatcher.ForwardBulletSpawn, GameMatcher.Forward,
            GameMatcher.LastShootTime
        ));

        currentTime = 0.0f;
    }

    public void Execute()
    {
        currentTime += Time.deltaTime;
        foreach (GameEntity spawn in group)
        {
            if (CouldSpawnBullet(spawn))
            {
                SpawnBullets(spawn);
                spawn.ReplaceLastShootTime(currentTime);
            }
        }
    }

    private bool CouldSpawnBullet(GameEntity entity)
    {
        float cooldown = GetCooldown(entity);
        return currentTime > entity.lastShootTime.value + cooldown;
    }

    private float GetCooldown(GameEntity entity)
    {
        if (entity.isPlayer)
        {
            return 0.1f;
        }
        return entity.hasBulletCooldown ? entity.bulletCooldown.cooldown : 0.5f;
    }

    private void SpawnBullets(GameEntity spawnEntity)
    {
        IBulletBuilder builder = new ForwardBulletBuilder();
        builder.SetContext(contexts);
        builder.SetCreator(spawnEntity);

        GameEntity newBullet = contexts.game.CreateEntity();
        builder.Build(newBullet);
    }
}
