﻿using System.Collections.Generic;
using Entitas;
using UnityEngine;

namespace Systems.SpwanSystems
{
    public class SpawnEnemyFromWaveSystem : IExecuteSystem
    {
        private GameContext gameContext;
        private EnemyFactory enemyFactory;
        private List<EnemyWave> spawnWave;
        
        public SpawnEnemyFromWaveSystem(Contexts contexts)
        {
            gameContext = contexts.game;
            spawnWave = contexts.game.spawnWave.currentSpawningwaves;
            enemyFactory = new EnemyFactory();
        }

        public void Execute()
        {
            float deltaTime = Time.deltaTime;
            
            foreach (EnemyWave spawnWave in spawnWave)
            {
                if (spawnWave.ShallSpawnEnemy(deltaTime))
                {
                    GameEntity entity = enemyFactory.ChainBuildEneme(gameContext, spawnWave);
                    entity.AddResource(spawnWave.enemyblueprint.view);
                    entity.isEnemy = true;
                    entity.AddForward(Vector3.down);
                    entity.AddLastShootTime(0);
                }
            }
            
            RmoveOutdatedWaves();
        }
        
        
        
        private void RmoveOutdatedWaves()
        {
            for (int i = spawnWave.Count -1; i>= 0; i--)
            {
                if (spawnWave[i].IsWaveFinished())
                {
                    spawnWave.RemoveAt(i);
                }
            }
        }
    }
}