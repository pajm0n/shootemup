﻿using Entitas;
using UnityEngine;

public class SpawnGroundTilesSystem : IInitializeSystem, IExecuteSystem
{
    private readonly Contexts contexts;
    private readonly MapGeneratorConfig mapConfig;
    private readonly Camera cam;
    
    private GameEntity lastEntityCreated;

    public SpawnGroundTilesSystem(Contexts contexts)
    {
        this.contexts = contexts;
        mapConfig = contexts.game.mapGeneratorConfig.value;
        cam = Camera.main;
    }

    public void Initialize()
    {
        CreateMapTile(0);
    }

    public void Execute()
    {
        if (GetObjectYRelativeToCameraSpace() <= 1f)
        {
          CreateMapTile(lastEntityCreated.position.Position.y + mapConfig.GetMeshSize());
        }
        
    }
    
    private float GetObjectYRelativeToCameraSpace()
    {
        return cam.WorldToViewportPoint(lastEntityCreated.position.Position).y;
    }
    
    private void CreateMapTile(float spawPoint)
    {
        GameEntity entity = contexts.game.CreateEntity();
        entity.AddResource(mapConfig.GetRandomMap());
        entity.AddDirection(Vector3.down);
        entity.AddVelocity(mapConfig.mapScrollingSpeed);
        entity.AddPosition(new Vector3(0, spawPoint, 0));
        lastEntityCreated = entity;
    }
}