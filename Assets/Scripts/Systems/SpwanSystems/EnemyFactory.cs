﻿using System.Collections.Generic;
using Entitas;
using Tools;
using UnityEngine;

namespace Systems.SpwanSystems
{
    public class EnemyFactory
    {
        public GameEntity ChainBuildEneme(GameContext context, EnemyWave wave)
        {

            GameEntity enemy = context.CreateEntity();
            foreach (var component in wave.enemyblueprint.components)
            {
                switch (component.compnent)
                {
                    case Components.BezierMovement:
                        foreach (BezierCurve curve in wave.enemyblueprint.path)
                        {
                            if (enemy.hasEnemyPath)
                            {
                                enemy.enemyPath.path.AddRange(BezierGenerator.CreateBezier(0.03f, curve));
                                continue;
                            }

                            enemy.AddEnemyPath(BezierGenerator.CreateBezier(0.03f, curve), 0, 0);
                            enemy.AddPosition(new Vector3(curve.p1.x, curve.p1.y, -1.0f));
                        }
                        enemy.AddVelocity(0);
                        break;
                    case Components.ChangeableSpeed:

                        var keyFrames = NormalizeAllAnimationCurvesToSingle(wave);

                        enemy.AddChangableSpeed(new AnimationCurve(keyFrames.ToArray()), wave.enemyblueprint.speed);
                        break;
                    case Components.PlainSpeed:
                        enemy.AddVelocity(wave.enemyblueprint.speed);
                        break;
                    case Components.PlainDirection:
                        break; //TODO
                    case Components.PlainSpawnPoint:
                        break; //TODO
                    case Components.Points:
                        string points = wave.enemyblueprint.GetComponentValueByEnum(Components.Points).value;
                        enemy.AddPointsOnDeath(float.Parse(points));
                        break;
                    case Components.Life:
                        string life = wave.enemyblueprint.GetComponentValueByEnum(Components.Life).value;
                        enemy.AddLife(new LifeStruct(float.Parse(life)));
                        break;
                    case Components.IsRepeatMovement:
                        enemy.isLoopingMovement = true;
                        break;
                    case Components.ForwardBulletSpawn:
                        enemy.isForwardBulletSpawn = true;
                        break;
                    case Components.ShotgunBulletSpawn:
                        enemy.isShotgunBulletSpawn = true;
                        break;
                    case Components.BulletCooldown:
                        enemy.AddBulletCooldown(float.Parse(component.value));
                        break;
                    case Components.BulletDamage:
                        enemy.AddDamage(float.Parse(component.value));
                        break;
                    case Components.BulletSpeed:
                        enemy.AddBulletSpeed(float.Parse(component.value));
                        break;
                    case Components.IsBoss:
                        enemy.isBoss = true;
                        break;
                    case Components.None:
                        break;
                }
            }

            return enemy;
        }

        private List<Keyframe> NormalizeAllAnimationCurvesToSingle(EnemyWave wave)
        {
            List<Keyframe> keyFrames = new List<Keyframe>();
            float curvesCount = wave.enemyblueprint.speedCurve.Count;
            for (int i = 0; i < curvesCount; i++)
            {
                Keyframe[] keys = wave.enemyblueprint.speedCurve[i].keys;
                foreach (Keyframe key in keys)
                {
                    var keyframe = key;
                    keyframe.time = i / curvesCount + keyframe.time / curvesCount;
                    keyFrames.Add(keyframe);
                }
            }

            return keyFrames;
        }
    }
}