﻿using UnityEngine;
using Entitas;

public class PlayerSpawnSystem : IInitializeSystem
{
    private readonly Contexts contexts;

    public PlayerSpawnSystem(Contexts contexts)
    {
        this.contexts = contexts;
    }

    public void Initialize()
    {        
        GameEntity playerEntity = contexts.game.CreateEntity();
        playerEntity.AddPosition(new Vector3(0, -3.5f, -1.0f));
        playerEntity.AddVelocity(contexts.game.playerConfig.value.playerSpeed);
        playerEntity.AddDirection(Vector3.zero);

        playerEntity.isForwardBulletSpawn = true;
        playerEntity.AddLastShootTime(0);
        playerEntity.AddResource(contexts.game.playerConfig.value.playerPrefab);
        playerEntity.isPlayer = true;
        playerEntity.AddLife(new LifeStruct(contexts.game.playerConfig.value.playerLife));
        playerEntity.AddForward(Vector3.up);
    }
}