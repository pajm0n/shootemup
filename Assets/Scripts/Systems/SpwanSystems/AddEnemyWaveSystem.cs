﻿using System.Collections.Generic;
using Entitas;
using UnityEngine;

namespace Systems.SpwanSystems
{
    public class AddEnemyWaveSystem : IExecuteSystem
    {
        private GameContext context;
        private IGroup<GameEntity> isBossGroup;

        private List<EnemyWave> copiedWaves = new List<EnemyWave>();
        private float timeSinceStartup = 0;

        public AddEnemyWaveSystem(Contexts contexts)
        {
            context = contexts.game;

            //16.12.2019 szmury copy waves so they won't disapear form gameobject
            foreach (var enemyWave in context.scenarioScript.value.EnemyWaves)
            {
                copiedWaves.Add(enemyWave);
            }

            isBossGroup = contexts.game.GetGroup(GameMatcher.Boss);
        }

        public void Execute()
        {

            if (isBossGroup.count > 0)
            {
                Debug.LogError(isBossGroup.count);
                return;
            }

            timeSinceStartup += Time.deltaTime;
            AddWave();
        }

        private void AddWave()
        {
            foreach (EnemyWave enemyWave in copiedWaves)
            {
                if (!enemyWave.isSpawned && enemyWave.firstSpawnTimeSeconds <= timeSinceStartup)
                {
                    enemyWave.isSpawned = true;
                    context.spawnWave.currentSpawningwaves.Add(enemyWave);
                }
            }
        }
    }
}