﻿using System.Collections.Generic;
using Entitas;
using UnityEngine;

public class PlayerLifeLostSystem : ReactiveSystem<GameEntity>
{
    private readonly Contexts contexts;

    public PlayerLifeLostSystem(Contexts contexts) : base(contexts.game)
    {
        this.contexts = contexts;
    }


    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.AllOf(GameMatcher.Life, GameMatcher.Player));
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.isPlayer && entity.hasLife;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        LifeStruct lifeStruct = contexts.game.playerEntity.life.life;
        contexts.game.playerEntity.ReplacePlayerLifeLost(lifeStruct);
    }
}