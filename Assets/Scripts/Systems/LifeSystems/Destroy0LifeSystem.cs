﻿using System.Collections.Generic;
using Entitas;
using UnityEngine;


public class Destroy0LifeSystem : ReactiveSystem<GameEntity>
{
    Contexts contexts;

    public Destroy0LifeSystem(Contexts contexts) : base(contexts.game)
    {
        this.contexts = contexts;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.Life);
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.hasLife;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (GameEntity e in entities)
        {
            if (e.life.life.currentLife <= 0)
            {
                if (e.hasPosition)
                {
                    IEntityFactory effect = new DestroyEffectFactory();
                    effect.Create(contexts, e);
                }

                e.isDestroy = true;
                e.isDestroyedByPlayer = true;
            }
        }
    }
}