﻿using System;
using Entitas;
using UnityEngine;

public class DestroyEffectFactory : IEntityFactory
{
    public GameEntity Create(Contexts contexts, GameEntity creator)
    {
        GameEntity e = contexts.game.CreateEntity();
        e.AddPosition(creator.position.Position);
        e.AddVelocity(0);
        e.AddDirection(Vector3.zero);
        e.AddResource(contexts.game.gameSetup.value.explosionEffect);
        e.AddDestroyAfterTime(3.0f);
        return e;
    }

    public GameEntity Create(Contexts contexts)
    {
        return null;
    }
}
