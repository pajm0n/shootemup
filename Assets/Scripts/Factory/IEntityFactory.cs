﻿using System;
using Entitas;

public interface IEntityFactory
{
    GameEntity Create(Contexts contexts);
    GameEntity Create(Contexts contexts, GameEntity creator);
}
