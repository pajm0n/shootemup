﻿using System.Collections.Generic;
using Tools;
using Ui;
using UnityEngine;


public class Controller : MonoBehaviour
{
    public GameSetup gameSetup;
    public PlayerConfig playerConfig;
    public MapGeneratorConfig mapGeneratorConfig;
    public ScenarioScript ScenarioScript;
    
    [SerializeField] private UiHolder uiHolder;
    
    private Entitas.Systems systems;

    private void Awake()
    {
        Contexts contexts = Contexts.sharedInstance;
        contexts.game.SetGameSetup(gameSetup);
        contexts.game.SetSpawnWave(new List<EnemyWave>());
        
        contexts.game.SetPlayerConfig(playerConfig);
        contexts.game.SetMapGeneratorConfig(mapGeneratorConfig);
        contexts.game.SetScenarioScript(ScenarioScript);
        contexts.game.SetAccumulatedPoints(0);
                    
        CreateSystems(contexts);                
        systems.Initialize();
        
        //TODO Szmury 23.10.2019 Move it somwhere else to different class
        uiHolder.lifeBar.RegisterToEntity(contexts.game.playerEntity);
        uiHolder.pointsShower.RegisterToEntity(contexts.game.accumulatedPointsEntity);
        contexts.game.playerEntity.life.life.currentLife -= 10;

    }

    private void Update()
    {
        systems.Execute();
        systems.Cleanup();
    }

    private void CreateSystems(Contexts contexts)
    {
        systems = new Feature("root")
            .Add(new FirstFeature(contexts))
            .Add(new InputFeature(contexts));
    }
}