﻿using Entitas;
using UnityEngine;


//CollisionEventComponent
public class CollisionComponent : IComponent
{
    public GameObject firstObject;
    public GameObject secondObject;
}

//ColisionApplyComponent
public class CollidedComponent : IComponent
{
    public GameEntity other;
}