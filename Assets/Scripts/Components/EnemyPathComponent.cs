﻿using System.Collections.Generic;
using Entitas;
using UnityEngine;

[Game]
public class EnemyPathComponent : IComponent
{
    public List<Vector2> path;
    //Can be moved to a seperate component in future and system Szmury 18.12/2019
    public float distanceTraveled;
    public float WholeDistanceToTravel = 0;  // It can be optimzed?? perhaps??
    public bool isLastMovement => distanceTraveled >= WholeDistanceToTravel;
    
    
    public Vector3 GetMovementDirection(float distanceThisFrame, Vector2 currentPos)
    {
        if (WholeDistanceToTravel == 0)
        {
            for (int i = 0; i < path.Count - 1; i++)
            {
                WholeDistanceToTravel += Vector2.Distance(path[i],path[i + 1]);
            }
        }
        
        distanceTraveled += distanceThisFrame;
        float accumulationOfDoistance = 0;
 
        Vector2 destinedPos = path[0];        
        for(int i = 0; i< path.Count; i++)
        {
            if (accumulationOfDoistance > distanceTraveled)
                break;
            
            Vector2 point = path[i];
            accumulationOfDoistance += Vector2.Distance(destinedPos, point);
            destinedPos = point;
        }

        return (destinedPos - currentPos).normalized;
    }

    public Vector3 GetLastDirection()
    {
        return (path[path.Count - 1] - path[path.Count - 2] ).normalized;
    }
}