﻿using Entitas;
using UnityEngine;

public class DirectionComponent : IComponent
{
   public Vector3 Direction;
}