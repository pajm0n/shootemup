﻿using Entitas;
using UnityEngine;
using Entitas.CodeGeneration.Attributes;

public class ViewComponent : IComponent
{
    [EntityIndex]
    public GameObject View;
}