﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game]
public class PointsOnDeathComponent : IComponent
{
    public float value;
}
[Game, Unique, Event(EventTarget.Self)]
public class AccumulatedPointsComponent : IComponent
{
    public float value;
}