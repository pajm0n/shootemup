﻿using System.Collections.Generic;
using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Unique]
public class SpawnWaveComponent : IComponent
{
    public List<EnemyWave> currentSpawningwaves;
}