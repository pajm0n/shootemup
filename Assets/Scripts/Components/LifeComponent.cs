﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game]
public class LifeComponent : IComponent
{
    public LifeStruct life;
}

[Unique, Game, Event(EventTarget.Self, EventType.Added)]
public class PlayerLifeLostComponent : IComponent
{
    public LifeStruct life;
}

public struct LifeStruct
{
    public LifeStruct(float currentLife)
    {
        this.currentLife = currentLife;
        maxLife = currentLife;
    }

    public float currentLife;
    public float maxLife;

    public float PercentageLife
    {
        get { return currentLife / maxLife; }
    }
}