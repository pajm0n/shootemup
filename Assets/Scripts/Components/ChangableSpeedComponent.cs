﻿using Entitas;
using UnityEngine;

[Game]
public class ChangableSpeedComponent : IComponent
{
    public AnimationCurve speedCurve;
    public float standardSpeed;

    private const float MIN_SPEED_EVALUATION = 1;

    public float GetCurrentSpeed(float currentDistancetraveled, float maxDistance)
    {
        return (speedCurve.Evaluate(currentDistancetraveled / maxDistance)  + MIN_SPEED_EVALUATION) 
               * standardSpeed;
    }
}

