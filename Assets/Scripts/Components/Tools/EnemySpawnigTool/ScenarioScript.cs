﻿using System.Collections.Generic;
using Entitas.CodeGeneration.Attributes;
using UnityEngine;

[CreateAssetMenu]
[Game, Unique]
public class ScenarioScript : ScriptableObject
{
    public List<EnemyWave> EnemyWaves;
}
