﻿using System;
using System.Collections.Generic;
using Tools;
using UnityEngine;

[CreateAssetMenu]
public class EnemyWave : ScriptableObject
{
    [Header("WaveStats")] public int enemiesCount;
    public float spawnTimeBetweenEnemeise;
    public float firstSpawnTimeSeconds;
    [NonSerialized] public bool isSpawned = false;

    [Header("Enemy")] public EnemyBlueprint enemyblueprint;

    [NonSerialized] private float lastTimeenemySpawn = 0;
    [NonSerialized] private float accumulatedTime = 0;
    
    public bool ShallSpawnEnemy(float currentTime)
    {
        accumulatedTime += currentTime;
        if (lastTimeenemySpawn < accumulatedTime)
        {
            lastTimeenemySpawn += spawnTimeBetweenEnemeise;
            return true;
        }

        return false;
    }

    public bool IsWaveFinished()
    {
        return accumulatedTime >= (enemiesCount -1) * spawnTimeBetweenEnemeise;
    }
}

[Serializable]
public class EnemyBlueprint
{
    public GameObject view;
    public List<ComponentValue> components;
    public List<BezierCurve> path;
    public List<AnimationCurve> speedCurve;
    public float speed;


    public ComponentValue GetComponentValueByEnum(Components component)
    {
        return components.Find(x => x.compnent == component);
    }
}

[Serializable]
public class ComponentValue
{
    public Components compnent;
    public string value;
}


public enum Components
{
    BezierMovement = 0,
    ChangeableSpeed = 1,
    PlainSpeed = 2,
    PlainDirection = 3,
    PlainSpawnPoint = 4,
    Points = 5,
    Life = 6,
    IsBoss = 7,
    ForwardBulletSpawn = 8,
    BulletSpeed = 9,
    BulletCooldown = 10,
    BulletDamage = 11,
    ShotgunBulletSpawn = 12,
    IsRepeatMovement = 13,
   
    None = 9999
}