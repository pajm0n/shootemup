﻿using System;
using UnityEngine;

namespace Tools
{
    [Serializable]
    public class BezierCurve : ScriptableObject
    {
        public Vector2 p1;
        public Vector2 p2;
        public Vector2 p3;
        public Vector2 p4;

        public void Construct(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4)
        {
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
            this.p4 = p4;
        }
    }
}