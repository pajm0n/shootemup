﻿using Entitas.CodeGeneration.Attributes;
using UnityEngine;

namespace Tools
{
    [CreateAssetMenu]
    [Game, Unique]
    public class BezierCurveHolder : ScriptableObject
    {
        public BezierCurve FirstCurve;
    }
}