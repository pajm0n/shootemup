﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Tools
{
    public static class BezierGenerator
    {
        public  static List<Vector2> CreateBezier(float dt, Vector2 pt0, Vector2 pt1, Vector2 pt2, Vector2 pt3)
        {
            // Draw the curve.
            List<Vector2> points = new List<Vector2>();
            for (float t = 0.0f; t < 1.0; t += dt)
            {
                points.Add(new Vector2(
                    CreteFloat(t, pt0.x, pt1.x, pt2.x, pt3.x),
                    CreteFloat(t, pt0.y, pt1.y, pt2.y, pt3.y)));
            }

            // Connect to the final point.
            points.Add(new Vector2(
                CreteFloat(1.0f, pt0.x, pt1.x, pt2.x, pt3.x),
                CreteFloat(1.0f, pt0.y, pt1.y, pt2.y, pt3.y)));

            return points;
        }
        
        private static float CreteFloat(float t,
            float x0, float x1, float x2, float x3)
        {
            return (
                x0 * Mathf.Pow((1 - t), 3) +
                x1 * 3 * t * Mathf.Pow((1 - t), 2) +
                x2 * 3 * Mathf.Pow(t, 2) * (1 - t) +
                x3 * Mathf.Pow(t, 3)
            );
        }

        public static List<Vector2> CreateBezier(float dt, BezierCurve curve)
        {
            return CreateBezier(dt, curve.p1, curve.p2, curve.p3, curve.p4);
        }

     

    }
}