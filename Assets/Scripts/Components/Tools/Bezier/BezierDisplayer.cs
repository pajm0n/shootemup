﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

namespace Tools
{
    [ExecuteInEditMode]
    public class BezierDisplayer : MonoBehaviour
    {
        public float ControlPointSize;
        public float StepPointSize;
        public float ZPosOfPoints;
        [Range(0.03f, 1f)] public float BezierPrecision = 0.03f;

        public BezierCurve bezierCurveAsset;
        public Vector2 point0;
        public Vector2 point1;
        public Vector2 point2;
        public Vector2 point3;

        private List<Vector2> path;
        private BezierCurve previousCurve;
        
        private void Update()
        {
            DrawPoints(point0, Color.green, ControlPointSize);
            DrawPoints(point1, Color.green, ControlPointSize);
            DrawPoints(point2, Color.green, ControlPointSize);
            DrawPoints(point3, Color.green, ControlPointSize);

            if (path == null)
                return;

            foreach (var point in path)
            {
                DrawPoints(point, Color.red, StepPointSize);
            }
            
            for (int i =0;  i < path.Count; i++)
            {
                DrawPoints(path[i], Color.Lerp(Color.green, Color.red, (float)i/ path.Count), StepPointSize);
            }
        }

        public BezierCurve GetBezier()
        {          
            bezierCurveAsset = ScriptableObject.CreateInstance<BezierCurve>();
            bezierCurveAsset.Construct(point0, point1, point2, point3);
            return bezierCurveAsset;
        }

        private void OnValidate()
        {
            if (AreCurvesDifferent())
            {
                point0 = bezierCurveAsset.p1;
                point1 = bezierCurveAsset.p2;
                point2 = bezierCurveAsset.p3;
                point3 = bezierCurveAsset.p4;
                bezierCurveAsset.Construct(point0, point1, point2, point3);
            }

            path = BezierGenerator.CreateBezier(BezierPrecision, point0, point1, point2, point3);
            previousCurve = bezierCurveAsset;
        }

        private bool AreCurvesDifferent()
        {
            return bezierCurveAsset != null && bezierCurveAsset != previousCurve;
        }

        private void DrawPoints(Vector2 point, Color color, float precision)
        {
            Vector3 horizontalStart = new Vector3(point.x - precision, point.y, ZPosOfPoints);
            Vector3 horizontalend = new Vector3(point.x + precision, point.y, ZPosOfPoints);

            Vector3 verticalStart = new Vector3(point.x, point.y + precision, ZPosOfPoints);
            Vector3 verticalEnd = new Vector3(point.x, point.y - precision, ZPosOfPoints);

            Debug.DrawLine(horizontalStart, horizontalend, color);
            Debug.DrawLine(verticalStart, verticalEnd, color);
        }
    }
}