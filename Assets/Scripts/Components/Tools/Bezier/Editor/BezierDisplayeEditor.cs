﻿using Tools;
using UnityEditor;
using UnityEngine;


[CustomEditor(typeof(BezierDisplayer))]
public class BezierDisplayeEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        BezierDisplayer castedTarget = target as BezierDisplayer;

        if (GUILayout.Button("saveBezier"))
        {
            AssetDatabase.CreateAsset(castedTarget.GetBezier(),
                "Assets/Configs/BezierCurves/" + castedTarget.gameObject.name + ".Asset");
        }
    }
}