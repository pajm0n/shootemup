﻿using UnityEngine;

namespace Ui
{
    public class UiHolder : MonoBehaviour
    {
        public LifeBar lifeBar;
        public PointsShower pointsShower;
    }
}