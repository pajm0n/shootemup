﻿using UnityEngine;
using UnityEngine.UI;

public class LifeBar : MonoBehaviour, IPlayerLifeLostListener, IGameEventSubscriber
{
    [SerializeField] private Image currentLife;

    public void RegisterToEntity(GameEntity gameEntity)
    {
        gameEntity.AddPlayerLifeLostListener(this);
    }

    public void OnPlayerLifeLost(GameEntity entity, LifeStruct life)
    {
        currentLife.color = Color.Lerp(Color.red, Color.green, life.PercentageLife);
        currentLife.rectTransform.anchorMax = new Vector2(life.PercentageLife, currentLife.rectTransform.anchorMax.y);
    }
}