﻿using TMPro;
using UnityEngine;

namespace Ui
{
    public class PointsShower : MonoBehaviour, IAccumulatedPointsListener, IGameEventSubscriber
    {
        [SerializeField] private TextMeshProUGUI pointsShower;
        
        public void OnAccumulatedPoints(GameEntity entity, float value)
        {
            pointsShower.text = "Points: " + value;
        }

        public void RegisterToEntity(GameEntity gameEntity)
        {
            gameEntity.AddAccumulatedPointsListener(this);
        }
    }
}