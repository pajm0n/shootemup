﻿using UnityEngine;

public class EmitTriggerEntityBehaviour : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        var entity = Contexts.sharedInstance.game.CreateEntity();
        entity.AddCollision(gameObject, other.gameObject);
    }
}