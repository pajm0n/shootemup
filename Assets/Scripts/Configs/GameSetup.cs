﻿using Entitas.CodeGeneration.Attributes;
using UnityEngine;

[CreateAssetMenu]
[Game, Unique]
public class GameSetup : ScriptableObject
{
    public GameObject bullet;
    public GameObject enemyBullet;
    public GameObject explosionEffect;
    public float enemySpawnTime = 1.0f;
    public float enemySpeed = 1.0f;
    public float bulletSpeed = 20.0f;
    public float bulletSpawnTime = 1.0f;
}
