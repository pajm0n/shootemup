﻿using Entitas.CodeGeneration.Attributes;
using UnityEngine;

[CreateAssetMenu]
[Game, Unique]
public class PlayerConfig : ScriptableObject
{
    public GameObject playerPrefab;
    public float playerSpeed;
    public float playerLife;
}