﻿using Entitas.CodeGeneration.Attributes;
using UnityEngine;
using Random = UnityEngine.Random;

[CreateAssetMenu]
[Game, Unique]
public class MapGeneratorConfig : ScriptableObject
{
    [SerializeField] private GameObject[] mapTiles;
    
    public float mapScrollingSpeed;
    public float InitalYSpawnPoint;
  
    public GameObject GetRandomMap()
    {
        return mapTiles[Random.Range(0, mapTiles.Length)];
    }

    public float GetMeshSize()
    {
        return mapTiles[0].gameObject.GetComponent<MeshRenderer>().bounds.size.y;
    }
}