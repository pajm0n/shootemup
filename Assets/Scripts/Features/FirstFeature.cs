﻿using Systems.CollisionSystems;
using Systems.DestroySystems;
using Systems.MovementSystems;
using Systems.SpwanSystems;

public class FirstFeature : Feature
{
    public FirstFeature(Contexts contexts)
    {
        Add(new SpawnGroundTilesSystem(contexts));
        Add(new PlayerSpawnSystem(contexts));
        Add(new AddEnemyWaveSystem(contexts));
        Add(new SpawnEnemyFromWaveSystem(contexts));
        Add(new InstantiateViewSystem(contexts));
        
        EnemySystems(contexts);

        Add(new PlayerPositioningSystem(contexts));
        Add(new MovementSystem(contexts));
        Add(new DestroyOutOfBounds(contexts));
        //Add(new ReplaceForwardSystem(contexts)); Check model export direction!
        Add(new ForwardBulletSpawnSystem(contexts));
        Add(new ShotgunBulletSpawnSystem(contexts));
        
        //TODO Szmury 23.10.2019 move to other feature
        //TODO Szmury 23.10.2019 move to other feature
        Add(new AnyCollisionSystem(contexts));
        Add(new BulletCollisionSystem(contexts));
        Add(new ObjectWithLifeCollisionSystem(contexts));
        
        Add(new Destroy0LifeSystem(contexts));
        Add(new PlayerLifeLostSystem(contexts));

        Add(new PlayerLifeLostEventSystem(contexts));
        Add(new AccumulatedPointsEventSystem(contexts));
        
        Add(new OnEnemyWithPointsDestroyed(contexts));
        Add(new DestroyAfterTimeSystem(contexts));
        Add(new DestroySystem(contexts));
        
    }

    private void EnemySystems(Contexts contexts)
    {
        Add(new MovementLoopPathSystem(contexts));
        Add(new EnemyPathDirectionSystem(contexts));
        Add(new EnemyChangableSpeedSystem(contexts));
    }
}