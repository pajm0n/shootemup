﻿public interface IGameEventSubscriber
{
    void RegisterToEntity(GameEntity gameEntity);
}