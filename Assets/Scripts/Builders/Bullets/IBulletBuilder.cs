﻿using System;
using Entitas;

public interface IBulletBuilder
{
    void SetContext(Contexts contexts);
    void SetCreator(GameEntity creator);
    GameEntity Build(GameEntity entity);
}
