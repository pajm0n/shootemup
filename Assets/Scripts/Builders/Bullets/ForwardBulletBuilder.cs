﻿public class ForwardBulletBuilder : IBulletBuilder
{   
    private GameEntity creator;
    private Contexts contexts;

    private const float STANDARD_DAMAGE = 10;
    
    public GameEntity Build(GameEntity entity)
    {
        SetUpView(entity);
        SetUpDamage(entity);
        SetUpVelocity(entity);
        entity.AddPosition(creator.position.Position);
        entity.AddDirection(creator.forward.value);
        entity.AddForward(creator.forward.value);
        entity.isBullet = true;
        return entity;
    }

    public void SetContext(Contexts contexts)
    {
        this.contexts = contexts;
    }

    public void SetCreator(GameEntity creator)
    {
        this.creator = creator;
    }

    private void SetUpVelocity(GameEntity entity)
    {
        float speed = creator.hasBulletSpeed ? creator.bulletSpeed.speed : contexts.game.gameSetup.value.bulletSpeed;
        entity.AddVelocity(speed);
    }

    private void SetUpDamage(GameEntity entity)
    {

        float damage = creator.hasDamage ? creator.damage.damage : STANDARD_DAMAGE;
        if (creator.isPlayer)
        {
            damage = STANDARD_DAMAGE;
        }
        entity.AddDamage(damage);
    }

    private void SetUpView(GameEntity entity)
    {
            entity.AddResource(creator.isPlayer ? 
                contexts.game.gameSetup.value.bullet : contexts.game.gameSetup.value.enemyBullet);
  
    }
}