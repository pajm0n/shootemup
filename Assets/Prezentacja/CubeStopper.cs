﻿using System.Collections;
using System.Collections.Generic;
using Prezentacja;
using UnityEngine;

public class CubeStopper : MonoBehaviour
{
    [SerializeField] private MoveLeft firstCube;
    [SerializeField] private MoveRight secondCube;
    [SerializeField] private MoveUp thirdCube;
    [SerializeField] private MoveLeft fourthCube;
    [SerializeField] private MoveUp fourthCubeUp;

    [SerializeField] private ICubeMover exampleCubeMover;

    private ICubeMover[] cubeMovers;
    
    // Start is called before the first frame update
    void Start()
    {
        cubeMovers = GetComponentsInChildren<ICubeMover>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
//            firstCube.enabled = false;
//            secondCube.enabled = false;
//            thirdCube.enabled = false;
//            fourthCube.enabled = false;
//            fourthCubeUp.enabled = false;
           
            
            foreach (var cubeMober in cubeMovers)
            {
                cubeMober.SetEnabled(false);
            }
        }
    }
}


//AnotherObject - Singleton
//SomeDataholder - Singleton

//someGamobject.transform.GetChild(2).gameObject.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text =
//AnotherObject.Get().SomeValue.ToString() + "/" + SomeDataholder.Instance.ChangedMethodName


//someGamobject.transform.GetChild(1).gameObject.SetActive(false);
//someGamobject.transform.GetChild(2).gameObject.SetActive(true);















