﻿using UnityEngine;

namespace Prezentacja
{
    public interface ICubeMover
    {
        void SetEnabled(bool isEnabled);
    }
}