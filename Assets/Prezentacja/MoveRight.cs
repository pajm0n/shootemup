﻿using System.Collections;
using System.Collections.Generic;
using Prezentacja;
using UnityEngine;

public class MoveRight : MonoBehaviour, ICubeMover
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += Vector3.right * Time.deltaTime;
    }

    public void SetEnabled(bool isEnabled)
    {
        this.enabled = isEnabled;
    }
}
