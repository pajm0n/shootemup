//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentContextApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameContext {

    public GameEntity playerConfigEntity { get { return GetGroup(GameMatcher.PlayerConfig).GetSingleEntity(); } }
    public PlayerConfigComponent playerConfig { get { return playerConfigEntity.playerConfig; } }
    public bool hasPlayerConfig { get { return playerConfigEntity != null; } }

    public GameEntity SetPlayerConfig(PlayerConfig newValue) {
        if (hasPlayerConfig) {
            throw new Entitas.EntitasException("Could not set PlayerConfig!\n" + this + " already has an entity with PlayerConfigComponent!",
                "You should check if the context already has a playerConfigEntity before setting it or use context.ReplacePlayerConfig().");
        }
        var entity = CreateEntity();
        entity.AddPlayerConfig(newValue);
        return entity;
    }

    public void ReplacePlayerConfig(PlayerConfig newValue) {
        var entity = playerConfigEntity;
        if (entity == null) {
            entity = SetPlayerConfig(newValue);
        } else {
            entity.ReplacePlayerConfig(newValue);
        }
    }

    public void RemovePlayerConfig() {
        playerConfigEntity.Destroy();
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public PlayerConfigComponent playerConfig { get { return (PlayerConfigComponent)GetComponent(GameComponentsLookup.PlayerConfig); } }
    public bool hasPlayerConfig { get { return HasComponent(GameComponentsLookup.PlayerConfig); } }

    public void AddPlayerConfig(PlayerConfig newValue) {
        var index = GameComponentsLookup.PlayerConfig;
        var component = (PlayerConfigComponent)CreateComponent(index, typeof(PlayerConfigComponent));
        component.value = newValue;
        AddComponent(index, component);
    }

    public void ReplacePlayerConfig(PlayerConfig newValue) {
        var index = GameComponentsLookup.PlayerConfig;
        var component = (PlayerConfigComponent)CreateComponent(index, typeof(PlayerConfigComponent));
        component.value = newValue;
        ReplaceComponent(index, component);
    }

    public void RemovePlayerConfig() {
        RemoveComponent(GameComponentsLookup.PlayerConfig);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherPlayerConfig;

    public static Entitas.IMatcher<GameEntity> PlayerConfig {
        get {
            if (_matcherPlayerConfig == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.PlayerConfig);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherPlayerConfig = matcher;
            }

            return _matcherPlayerConfig;
        }
    }
}
